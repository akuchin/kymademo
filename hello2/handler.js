const axios = require('axios');

module.exports = {
    main: async function (event, context) {
        console.log("Received add to cart event");
        let userId = event.data.userId;
        let cartId = event.data.cartId;
        let baseSiteUid = event.data.baseSiteUid;

        if (!userId) {
            console.log("Works only with logged in customers");
            return event.data
        }

        console.log("user: " + userId);
        console.log("cart: " + cartId);
        console.log("baseSiteUid: " + baseSiteUid);

        let host = process.env.OCC_GATEWAY_URL;

        let cart = (await axios.get(`${host}/${baseSiteUid}/users/${userId}/carts/${cartId}`)).data;

        console.log("Cart:  " + JSON.stringify(cart,undefined, 4));

        return event.data;
    }
}
