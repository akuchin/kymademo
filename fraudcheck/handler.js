const axios = require('axios');

module.exports = {
    main: async function (event, context) {
        console.log("Recive fraud check event");
        let orderCode = event.data.orderCode;
        let processCode = event.data.processCode;

        console.log("order: " + orderCode);
        console.log("processCode: " + processCode);

        let occHost = process.env.OCC_GATEWAY_URL;
        let adminHost = process.env.ADM_GATEWAY_URL;

        let order = (await axios.get(`${occHost}/electronics/orders/${orderCode}`)).data;
        console.log("Order: " + JSON.stringify(order, undefined, 4));

        let result = checkFraud(order);

        let fraud = {
            "choice": result,
            "event": `${processCode}_externalFraudCheckEvent`
        };

        // console.log("fr: " + JSON.stringify(fraud, undefined, 4));
        axios.post(`${adminHost}/businessprocess/events`, fraud);

        return event.data;
    }
}

function checkFraud(order) {
    if (order.totalPrice.value > 400)
        return "FRAUD";
    else
        return "OK";
}
